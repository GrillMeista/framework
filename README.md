## Installation

[Download Node.js](https://nodejs.org/en/)

git clone https://gitlab.com/GrillMeista/framework.git

npm install

./fm.sh init

## Features
node.js webserver build upon express 4<br>
simplified REST-routing with routing objects in each module<br>
sqlite3 database

### Project initialization
creates the project structure including node.js webserver and database structure file<br>
commands: **./fm.sh** **i** or **init**<br>

### Command overview
commands: **./fm.sh** **h** or **help**<br>

### Create database
Creates a Sqlite3 database from database/structure.sql<br>
commands: **./fm.sh** **db**, **bdb** or **build-database**<br>

### Create module
Creates a new module including REST-router and Database model<br>
parameters: module-name(mandatory), REST-route (optional)<br>
commands: **./fm.sh** **cm** or **create-module**, **nm** or **new-module**<br>

### Remove module
Removes an existing module from configurations and src/modules directory<br>
commands: **./fm.sh** **rm** or **remove-module**<br>

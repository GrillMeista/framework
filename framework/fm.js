const fs = require('fs');
const { resolve } = require('path');

let options = [
    {
        names: ['i', 'init'],
        function: init
    },
    {
        names: ['db', 'bdb', 'build-database'],
        function: buildDatabase
    },
    {
        names: ['cm', 'create-module', 'nm', 'new-module'],
        function: createModule
    },
    {
        names: ['rm', 'remove-module'],
        function: removeModule
    },
    {
        names: ['h', 'help'],
        function: help
    }
];

async function exists(path){
    return new Promise( resolve => {
        fs.access(path, err => {
            if(err){
                resolve(false);   
            }
            resolve(true);
        });
    });
}

async function read(filename){
    return new Promise( resolve => {
        fs.readFile( filename, {encoding: 'utf-8'}, (err, file) => {
            if(err){
                throw err;
            }else{
                resolve(file);
            }
        });
    });
}

(async _ => {
    options
        .find( o => o.names.includes(process.argv[2]) )
        .function(process.argv.slice(3));
})();

async function init(){
    let directories = [
        './src/modules', './src/config', './src/public', 
        './src/modules/application', 
        './database', './src/public/css', './src/public/js'
    ];
    let files = [
        './index.js', 
        './App.js', 
        './src/config/Application.json',
        './src/modules/application/Router.js',
        './src/modules/application/DataObject.js',
        './src/modules/application/DatabaseModel.js',
        './src/modules/application/DatabaseHandler.js',
        './src/public/index.html'
    ];

    for(let i = 0; i < directories.length; i++){
        if(!await exists(directories[i])){
            fs.mkdir(directories[i], { recursive: true }, err => {
                if(err) throw err;
            });
        }else{
            console.log(`${directories[i]} already exists.`);
            continue;
        }
    }

    for(let j = 0; j < files.length; j++){   
        if(!await exists(files[j])){
            fs.copyFile('./framework/sources/' + files[j].split('/').slice(-1), files[j], (err) => {});
        }else{
            console.log(`${files[j]} already exists.`);
            continue;
        }
    }

    [
        './src/public/js/index.js', 
        './src/public/css/index.css',
        './database/structure.sql'
    ].forEach(path => {
        fs.writeFile(path, '', err => {
            if(err){ throw err; }
        });
    })
}

async function buildDatabase(){
    const Database = require('better-sqlite3');
    const db = new Database('database/database.db');

    db.exec(fs.readFileSync('./database/structure.sql', 'utf8'));
}

async function createModule(options){
    let application = await read('./src/config/Application.json');
        application = JSON.parse(application);

    options[0] = options[0].toLowerCase()

    if(!await exists(options[0])){
        fs.mkdir('./src/modules/' + options[0], { recursive: true }, err => {
            if(err) throw err;
        });
    }

    let route = '/' + options[0];

    if(options[1] !== undefined){
        route = '/' + options[1];

    }else if(options[0].slice(-1) != 's'){
        route = '/' + options[0] + 's';

    }

    let routerName = options[0].slice(0, 1).toUpperCase() + options[0].slice(1) + 'Router';
    let modelName = options[0].slice(0, 1).toUpperCase() + options[0].slice(1) + 'Model';

    if( application.modules.find( m => m.name == options[0] ) == undefined ){
        application.modules.push({
            name: options[0],
            router: routerName,
            route: route
        });
    }

    fs.writeFile('./src/config/Application.json', JSON.stringify(application), err => {
        if(err){ throw err; }
    });

    let router = await read('./framework/templates/Router.js');
    let model  = await read('./framework/templates/Model.js');

    if(!await exists(`./src/modules/${options[0]}/${routerName}.js`)){
        router = router.replace(/{{RouterName}}/g, routerName);
        router = router.replace(/{{ModelName}}/g, modelName);

        fs.writeFile(`./src/modules/${options[0]}/${routerName}.js`, router, err => {
            if(err){ throw err; }
        })
    }

    if(!await exists(`./src/modules/${options[0]}/${modelName}.js`)){
        model = model.replace(/{{ModelName}}/g, modelName);

        fs.writeFile(`./src/modules/${options[0]}/${modelName}.js`, model, err => {
            if(err){ throw err; }
        });
    }
}

async function removeModule(options){
    let application = await read('./src/config/Application.json');
    application = JSON.parse(application);

    if(!await exists('./src/modules/' + options[0])){
        console.log(`Module ${options[0]} does not exist.`);
        return;
    }

    fs.rmdir('./src/modules/' + options[0], { recursive: true }, err => {
        if(err){ throw err; }
    });

    application.modules = application.modules.filter(
        module => module.name != options[0]
    );
    
    fs.writeFile('./src/config/Application.json', JSON.stringify(application), err => {
        if(err){ throw err; }
    });
}

async function help(){
    options.forEach( option => {
        console.log(option.names.join(', '))
    });
}
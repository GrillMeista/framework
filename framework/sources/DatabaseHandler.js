const Database = require('better-sqlite3');
//const db = new Database('./database/database.db');

module.exports = class DatabaseHandler {

    constructor(filepath){
        this.db = new Database(filepath);
    }

    async push(query, data = {}){
        this.db.prepare(query).run(data);
    }

    async pull(query, data = {}){
        return await this.db.prepare(query).get(data);
    }

    async pullList(query, data = {}){
        return await this.db.prepare(query).all(data);
    }

    async pushList(query, list = []){
        const statement = this.db.prepare(query);
        for(const element of list){
            statement.run(element);
        } 
    }

    async pullObject(query, data = {}, obj){
        await obj.fill(await this.db.prepare(query).get(data));
        return obj;
    }

    async pullComposition(query, data = {}, obj){
        await obj.merge(await this.db.prepare(query).all(data));
        return obj;
    }
    
    // Singleton
    static getInstance(filepath){
        if(DatabaseHandler.handler == undefined || DatabaseHandler.handler == null){
            DatabaseHandler.handler = new DatabaseHandler(filepath);
        }

        return DatabaseHandler.handler;
    }
}
const express = require('express');
const bodyParser = require('body-parser');
const config = require('./src/config/Application.json');

class App {
    constructor() {
        this.expressApp = express();
        this.middleware();
        this.routes();
    }
    
    middleware(){
        this.expressApp.use(bodyParser.json());
        this.expressApp.use(bodyParser.urlencoded({ extended: true }));
        this.expressApp.use('/', express.static('src/public'));
    }

    routes(){
        config.modules.forEach(m => {
            this.expressApp.use('/api' + m.route, require('./src/modules/' + m.name +  '/' + m.router));
        });
    }
}

module.exports = (new App()).expressApp;
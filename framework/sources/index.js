const http = require('http');
const Application = require('./App');
const port = 80;

Application.set('port', port);

const server = http.createServer(Application);

server.listen(port);
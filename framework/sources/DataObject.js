class DataObject {
    constructor() {
        this.init();
    }

    init(){}

    async fill(data = {}){
        for (let property in data){
            if(this[property] !== undefined){
                this[property] = data[property];
            }else{
                throw new Error("Submitted data doesn't fit object structure");
            }
        }
    }

    async merge(data = [{}]){
        for(let i = 0; i < data.length; i++){
            let subObject = undefined, subObjectName = "";

            for(let property in data[i]){
                subObjectName = property.split('_')[0] + 's';

                if(this[property] !== undefined || this[subObjectName] != undefined){
                    if(Array.isArray(this[property]) || Array.isArray(this[subObjectName])){
                        if(
                            this[subObjectName].length > 0 &&
                            !Array.isArray(this[subObjectName][0]) &&
                            typeof this[subObjectName][0] == "object"
                        ){
                            let subObjectProperties = property.split('_');

                            if(subObject == undefined){
                                subObject = {};
                            }

                            subObject[subObjectProperties[1]] = data[i][property];

                        }else{
                            this[property].push(data[i][property]);
                        }

                    }else if(i === 0){
                        this[property] = data[0][property];

                    }

                }else{
                    throw new Error("Submitted data doesn't fit object structure");

                }
            }

            if(subObject != undefined){
                this[subObjectName].push(subObject);
                subObject = undefined;

                if(i == 0){
                    this[subObjectName].shift();
                }
            }
        }
    }

    // async merge(data = [{}]){
    //     //console.log(data);
    //
    //     let subObjects = [], dataObjects = [];
    //
    //     for(let i = 0; i < data.length; i++){
    //         let dataObject = undefined, subObjectName = "";
    //
    //         for(let property in data[i]){
    //             //subObjectName = property.split('_')[0] + 's';
    //
    //             if(this[property] !== undefined || this[property.split('_')[0] + 's'] != undefined){
    //                 if(Array.isArray(this[property]) || Array.isArray(this[property.split('_')[0] + 's'])){
    //                     if(
    //                         this[property.split('_')[0] + 's'].length > 0 &&
    //                         !Array.isArray(this[property.split('_')[0] + 's'][0]) &&
    //                         typeof this[property.split('_')[0] + 's'][0] == "object"
    //                     ){
    //                         let subObjectProperties = property.split('_');
    //
    //                         if(dataObject == undefined){
    //                             dataObject = {};
    //                         }
    //
    //                         if(subObjects.find(sObj => sObj.name == property.split('_')[0] + 's') == undefined){
    //                             // subObjects
    //                             //     .find(sObj => sObj.name == property.split('_')[0] + 's')
    //                             //     .push(
    //                             //         {name: property.split('_')[0] + 's', data: []}
    //                             //     );
    //
    //                             subObjects.push({name: property.split('_')[0] + 's', data: []});
    //                         }
    //
    //                         if(subObjectName !== property.split('_')[0] + 's'){
    //                             //dataObjects.push({name: subObjectName, obj: dataObject});
    //
    //                             console.log(subObjectName, subObjects);
    //
    //                             subObjects
    //                                 .find(sObj => sObj.name == subObjectName)[0]
    //                                 .data
    //                                 .push(dataObject)
    //
    //                             dataObject = undefined;
    //
    //                             subObjectName = property.split('_')[0] + 's';
    //                         }
    //
    //                         dataObject[subObjectProperties[1]] = data[i][property];
    //
    //                     }else{
    //                         this[property].push(data[i][property]);
    //                     }
    //
    //                 }else if(i === 0){
    //                     this[property] = data[0][property];
    //
    //                 }
    //
    //             }else{
    //                 throw new Error("Submitted data doesn't fit object structure");
    //
    //             }
    //         }
    //
    //         // if(subObject != undefined){
    //         //     this[subObjectName].push(subObject);
    //         //     subObject = undefined;
    //         //
    //         //     if(i == 0){
    //         //         this[subObjectName].shift();
    //         //     }
    //         // }
    //
    //         // if(dataObject != undefined){
    //         //     subObjects
    //         //         .find(sObj => sObj.name == subObjectName)
    //         //         .data
    //         //         .push(dataObject);
    //         //
    //         //     dataObject = undefined;
    //         // }
    //     }
    //
    //     console.log(subObjects);
    //
    //     subObjects.forEach(subObject => {
    //         this[subObject.name] = subObject.data;
    //     });
    // }

    json(){
        return JSON.stringify(this);
    }
}

module.exports = DataObject;
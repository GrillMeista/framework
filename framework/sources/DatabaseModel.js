const DatabaseHandler = require('./DatabaseHandler');

class DatabaseModel {
    constructor() {
        this.handler = DatabaseHandler.getInstance('database/database.db');
    }
}

module.exports = DatabaseModel;
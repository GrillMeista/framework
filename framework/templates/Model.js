const DatabaseModel = require('../application/DatabaseModel');

class {{ModelName}} extends DatabaseModel {
    async getData(){
        let query = "";

        let data = await this.handler.pull(query, {});

        return data;
    }
}

module.exports = {{ModelName}};
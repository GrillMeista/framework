const Router = require('../application/Router');
const {{ModelName}} = require('./{{ModelName}}');

class {{RouterName}} extends Router {

    async get(req, res){
        res.status(200).json({success: true});
    }

    init(){
        this.router.get('/', this.get);
    }
    
}

module.exports = (new {{RouterName}}).router;